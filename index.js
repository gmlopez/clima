const express = require('express');
require('dotenv').config();
const cors = require('cors');

//crear el server de express
const app = express();


//CORS
app.use(cors());


app.use(express.json());


//eventos de clima
app.use('/v1', require('./routes/events'))



//directorio publico
app.use( express.static('public'));

//escuchar peticiones
app.listen(process.env.PORT, () => {
    console.log(`servidor corriendo en puerto ${process.env.PORT}` );
}) 
