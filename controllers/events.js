const { response } = require('express');
const axios = require('axios');
require('dotenv').config();

const obtenerCoordenadasPorLugar = async (req, res = response) => {

    const city = req;

    const url_clima = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${process.env.API_KEY}`;

    try {
        const response = await axios.get(url_clima);

        if(!response){
            return res.status(404).json({
                ok: false,
                msg: 'No hay coordenadas para esa ubicación'
            })
        }

        const {coord} = response.data;

        return coord;

      } catch (error) {
        console.error('error1:',error);

        res.status(500).json({
            ok:false,
            msg: 'Error!'
        });
      }

}

const obtenerCoordenadasPorIp = async (req, res = response) => {

    const ip = req;

    const url_clima = `http://ip-api.com/json`;

    try {
        const response = await axios.get(url_clima);

        if(!response){
            return res.status(404).json({
                ok: false,
                msg: 'No hay coordenadas para esa ubicación'
            })
        }

        const {lat, lon, city} = response.data;

        return {lat, lon, city};

      } catch (error) {
        console.error('error1:',error);

        res.status(500).json({
            ok:false,
            msg: 'Error!'
        });
      }

}

const Location = async (req, res = response) => {

    const city = req.params.city;
    let coord = {};

    try {
        if(!city){
            coord = await obtenerCoordenadasPorIp();
        }else{
            coord = await obtenerCoordenadasPorLugar(city);
        }

        if(!coord){
            return res.status(404).json({
                ok: false,
                msg: 'No hay coordenadas para esa ubicación'
            })
        }

        return res.json({
            ok: true,
            latitud: coord.lat,
            longitud: coord.lon,
            city: city
        });

      } catch (error) {
        console.error('error1:',error);

        res.status(500).json({
            ok:false,
            msg: 'Error!!'
        });
      }

}

const Current = async (req, res = response) => {

    let city = req.params.city;

    try {
        if(!city){
            const coordenadas = await obtenerCoordenadasPorIp();
            city = coordenadas.city;
        }

        const url_clima = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${process.env.API_KEY}`;
        console.log('url_clima',url_clima);
        const response = await axios.get(url_clima);
        console.log('res1: ',response);

        if(!response){
            return res.status(404).json({
                ok: false,
                msg: 'No hay coordenadas para esa ubicación'
            })
        }

        const {coord, weather} = response.data;

        return res.json({
            ok: true,
            latitud: coord.lat,
            longitud: coord.lon,
            clima: weather
        });

      } catch (error) {
        console.error('error1:',error);

        res.status(500).json({
            ok:false,
            msg: 'Error!'
        });
      }

}

const Forecast = async (req, res = response) => {

    const ciudad = req.params.city;
    let coord = {};

    try {
        if(!ciudad){
            coord = await obtenerCoordenadasPorIp();
        }else{
            coord = await obtenerCoordenadasPorLugar(ciudad);
        }

        if(!coord){
            return res.status(404).json({
                ok: false,
                msg: 'No hay coordenadas para esa ubicación'
            })
        }

        const {lon, lat, city} = coord;

        const url_clima = `http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&appid=${process.env.API_KEY}`;

        const response = await axios.get(url_clima);
        console.log('res1: ',city);

        if(!response){
            return res.status(404).json({
                ok: false,
                msg: 'No hay coordenadas para esa ubicación'
            })
        }

        const {list} = response.data;

        return res.json({
            ok: true,
            clima: list
        });

      } catch (error) {
        console.error('error1:',error);

        res.status(500).json({
            ok:false,
            msg: 'Error!'
        });
      }

}

module.exports = {
    Location,
    Current,
    Forecast
}