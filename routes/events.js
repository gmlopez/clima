const { Router }= require('express');
const router = Router();
const { Location, Current, Forecast } = require('../controllers/events');

    router.get('/location', 
    Location); 

    router.get('/current/:city', 
    Current); 

    router.get('/current', 
    Current); 

    router.get('/forecast/:city', 
    Forecast); 

    router.get('/forecast', 
    Forecast); 


module.exports = router;